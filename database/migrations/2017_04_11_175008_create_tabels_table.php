<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelsTable extends Migration
{
    public function up()
    {
        Schema::create('tables', function (Blueprint $table) {           
            $table->increments('id');
            $table->string('table_number')->unique(); 
            $table->string('status', 2)->dafault('Y');                       
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('tables');
    }
}
