<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');            
            $table->unsignedInteger('user_id')->nullable();            
            $table->string('order_number')->unique();         
            $table->unsignedInteger('table_id')->nullable(); 
            $table->decimal('sub_total');                
            $table->timestamp('order_date');
            $table->string('status', 2)->dafault('N');
        
        //Buat FK tanda dari mana asal kolom user_id
        
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        
        //Buat FK tanda dari mana asal kolom table_id
        
            $table->foreign('table_id')
                ->references('id')
                ->on('tables')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }    
}
