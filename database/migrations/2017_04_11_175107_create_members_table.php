<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_number');
            $table->string('name');
            $table->string('address');            
            $table->float('discount');  
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
