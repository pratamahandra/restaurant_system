<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{
   public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->increments('id');
            //kolom untuk foreign Key nya
            $table->unsignedInteger('user_id')->nullable();            
            $table->string('billing_number')->unique();         
            $table->decimal('sub_total');                     
            $table->decimal('delivery_amount');        
            $table->string('delivery_destination');                     
            $table->unsignedInteger('member_id')->nullable();
            $table->decimal('total');           
            $table->string('payment_method', 2);
            $table->timestamp('bill_date');
            $table->string('status', 2)->dafault('N');
      
        //Buat FK tanda dari mana asal kolom user_id      
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        //Buat FK tanda dari mana asal kolom table_id        
            $table->foreign('member_id')
                ->references('id')
                ->on('members')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });        
        
    }

    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
