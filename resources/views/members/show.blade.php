@extends('layouts.navbar')

@section('content')<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Edit Member
        </h1>      
    </div>
</div>

<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">Member {{ $member->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/members') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/members/' . $member->id . '/edit') }}" title="Edit Member"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
            'method'=>'DELETE',
            'url' => ['members', $member->id],
            'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Delete Member',
            'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $member->id }}</td>
                        </tr>
                        <tr><th> Card Number </th><td> {{ $member->card_number }} </td></tr><tr><th> Name </th><td> {{ $member->name }} </td></tr><tr><th> Adress </th><td> {{ $member->address }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
