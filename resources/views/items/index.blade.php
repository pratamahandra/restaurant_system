@extends('layouts.navbar')

@section('content') 
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Setup Food & Drink 
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->      
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">Create New Item</div>
        <div class="panel-body">                        
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif

            {!! Form::open(['url' => '/items', 'class' => 'form-horizontal', 'files' => true]) !!}

            @include ('items.form')

            {!! Form::close() !!}

        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">Items</div>
        <div class="panel-body">            
            {!! Form::open(['method' => 'GET', 'url' => '/items', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}         
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>No</th><th>Name</th><th>Category</th><th>Price</th><th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; ?>        
                        @foreach($items as $item)                                                        
                        <tr>
                        <td><?php echo $no++ ?></td> 
                            <td>{{ $item->name }}</td><td>@if ($item->category == 0)
                            Food                                
                            @else
                            Drink
                            @endif
                        </td>
                        <td>{{ $item->price }}</td>
                        <td>
                            <a href="{{ url('/items/' . $item->id) }}" title="View Item"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            <a href="{{ url('/items/' . $item->id . '/edit') }}" title="Edit Item"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/items', $item->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Item',
                            'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $items->appends(['search' => Request::get('search')])->render() !!} </div>
        </div>

    </div>
</div>
</div> 
@endsection
