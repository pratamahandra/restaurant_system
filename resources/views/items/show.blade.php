@extends('layouts.navbar')

@section('content')<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Show Food & Drink
        </h1>      
    </div>
</div>
<div class="col-md-9">
    <div class="panel panel-default">

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Item {{ $item->id }}</div>
                <div class="panel-body">

                    <a href="{{ url('/items') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/items/' . $item->id . '/edit') }}" title="Edit Item"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['items', $item->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Item',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $item->id }}</td>
                                </tr>
                                <tr><th> Name </th><td> {{ $item->name }} </td></tr><tr><th> Category </th><td> @if ($item->category == 0)
                                Food                                
                                @else
                                Drink
                                @endif
                                </td></tr><tr><th> Price </th><td> {{ $item->price }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
