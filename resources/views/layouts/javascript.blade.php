<script type="text/javascript">   
  function AddOrder(){
      //-------tambah row---------
      var row = '<tr>'+
      '<td>'+
      '<select class="food form-control" name="food[]">'+
      '<option value="0" selected="true" disabled="true">Select Food</option>'+   
      '@foreach($foods as $key => $p)'+
      '<option value="{!!$p->id!!}">{!!$p->name!!}</option>'+
      '@endforeach'+   
      '</select>'+
      '</td>'+
      '<td>'+
      '<input class="quantity form-control" name="quantity[]"></input>'+
      '</td>'+
      '<td>'+
      '<input class="price form-control" name="price[]" readonly="true"></input>'+
      '</td>'+
      '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>'+
      '</td>'+
      '</tr>';      
      $("#tbodyf").append(row);    
    };
    $('#tbodyf').delegate(".food","change",function(){
      var row = $(this).parent().parent();
      var id = row.find('.food').val();
      var dataId = {'id':id};
      $.ajax({
        type : 'GET',
        url : '{!!URL::route("findPrice")!!}',
        dataType : 'json',
        data : dataId,
        success : function(data){
          row.find('.price').val(data.price);            
        }
      });      
    });
    function deleteRow(btn) {
      var row = btn.parentNode.parentNode;
      row.parentNode.removeChild(row);
    };
    function AddOrderd(){
      //-------tambah row---------
      var rowd = '<tr>'+
      '<td>'+
      '<select class="drink form-control" name="drink[]">'+
      '<option value="0" selected="true" disabled="true">Select Drink</option>'+   
      '@foreach($drinks as $k => $d)'+
      '<option value="{!!$d->id!!}">{!!$d->name!!}</option>'+
      '@endforeach'+   
      '</select>'+
      '</td>'+
      '<td>'+
      '<input class="quantityd form-control" name="quantityd[]"></input>'+
      '</td>'+
      '<td>'+
      '<input class="priced form-control" name="priced[]" readonly="true"></input>'+
      '</td>'+
      '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>'+
      '</td>'+
      '</tr>';      
      $("#tbodyd").append(rowd);    
    };
    $('#tbodyd').delegate(".drink","change",function(){
      var rowd = $(this).parent().parent();
      var id = rowd.find('.drink').val();
      var dataId = {'id':id};
      $.ajax({
        type : 'GET',
        url : '{!!URL::route("findPrice")!!}',
        dataType : 'json',
        data : dataId,
        success : function(data){
          rowd.find('.priced').val(data.price);            
        }
      });      
    });
    function deleteRowd(btn) {
      var rowd = btn.parentNode.parentNode;
      rowd.parentNode.removeChild(rowd);
    };
    AddOrderd();    
    AddOrder();    

  </script>