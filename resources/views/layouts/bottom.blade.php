
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Restaurant System</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">                                            
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="/"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="/tables"><i class="fa fa-fw fa-eject"></i> Setup Table</a>
                    </li>
                    <li>
                        <a href="/items"><i class="fa fa-fw fa-cutlery"></i>Setup Food & Drink</a>
                    </li>
                    <li>
                        <a href="/orders"><i class="fa fa-fw fa-shopping-cart"></i>Setup Order</a>
                    </li>
                    <li>
                        <a href="/billings"><i class="fa fa-fw fa-usd"></i>Setup Billing</a>
                    </li>                                                        
                    <li>
                        <a href="/members"><i class="fa fa-fw fa-user"></i>Setup Member</a>
                    </li>                                                        
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>                               