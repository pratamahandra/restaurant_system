@extends('layouts.navbar')

@section('content')<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Show Table
        </h1>      
    </div>
</div>
<div class="col-md-9">
    <div class="panel panel-default">

        <div class="panel-body">

            <a href="{{ url('/tables') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/tables/' . $table->id . '/edit') }}" title="Edit Table"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
            'method'=>'DELETE',
            'url' => ['tables', $table->id],
            'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Delete Table',
            'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $table->id }}</td>
                        </tr>
                        <tr><th> Table Number </th><td> {{ $table->table_number }} </td></tr>
                        <tr><th> Table Status </th><td>@if ($table->status == 0)
                            Free
                            @elseif ($table->status == 1)
                            Ordered
                            @else
                            Reservation
                            @endif
                        </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection
