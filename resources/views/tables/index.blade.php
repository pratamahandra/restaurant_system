@extends('layouts.navbar')
@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Setup Table
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->                                
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">Create New Table</div>
        <div class="panel-body">                                
            <br />
            <br />

            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif

            {!! Form::open(['url' => '/tables', 'class' => 'form-horizontal', 'files' => true]) !!}

            @include ('tables.form')

            {!! Form::close() !!}

        </div>
    </div>
</div>

<div class="col-md-9">
        <div class="panel panel-default">  
            <div class="panel-heading">Table</div>            
            <div class="panel-body">
            {!! Form::open(['method' => 'GET', 'url' => '/tables', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}
                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="">
                        <thead>
                            <tr>
                                <th>No</th><th>Table Number</th><th>Status</th><th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>                        
                            <?php $no=1; ?>                     
                            @foreach($tables as $table)                                                                    
                            <tr>
                                <td><?php echo $no++ ?></td> 
                                <td>{{ $table->table_number }}</td>
                                <td>@if ($table->status == 0)
                                    Free
                                    @elseif ($table->status == 1)
                                    Ordered
                                    @else
                                    Reservation
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('/tables/' . $table->id) }}" title="View Table"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                    <a href="{{ url('/tables/' . $table->id . '/edit') }}" title="Edit Table"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/tables', $table->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Table',
                                    'onclick'=>'return confirm("Confirm delete?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper"> {!! $tables->appends(['search' => Request::get('search')])->render() !!} </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@endsection
