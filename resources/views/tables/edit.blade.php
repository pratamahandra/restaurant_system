@extends('layouts.navbar')
@section('content') 
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Dashboard <small>Setup Table</small>
        </h1>        
    </div>
</div>
<!-- /.row -->  
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">Edit Table #{{ $table->id }}</div>
        <div class="panel-body">
            <a href="{{ url('/tables') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif

            {!! Form::model($table, [
            'method' => 'PATCH',
            'url' => ['/tables', $table->id],
            'class' => 'form-horizontal',
            'files' => true
            ]) !!}

            @include ('tables.form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}

        </div>
    </div>
</div>        
@endsection
