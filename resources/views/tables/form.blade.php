<div class="form-group {{ $errors->has('table_number') ? 'has-error' : ''}}">
	{!! Form::label('table_number', 'Table Number', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-6">
		{!! Form::text('table_number', null, ['class' => 'form-control', 'required']) !!}
		{!! $errors->first('table_number', '<p class="help-block">:message</p>') !!}
	</div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Payment Method', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('status', ['Free', 'Ordered', 'Reservation'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group">
	<div class="col-md-offset-4 col-md-4">
		{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
	</div>
</div>
