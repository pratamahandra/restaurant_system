<div class="form-group {{ $errors->has('order_number') ? 'has-error' : ''}}">
    {!! Form::label('order_number', 'Order Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('order_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('order_number', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('table_id') ? 'has-error' : ''}}">
    {!! Form::label('table_id', 'Table Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('table_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('table_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('price', null, ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('order_date') ? 'has-error' : ''}}">
    {!! Form::label('order_date', 'Order Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('datetime-local', 'order_date', null, ['class' => 'form-control']) !!}
        {!! $errors->first('order_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
