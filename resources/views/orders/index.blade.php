<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Restaurant System</title>
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/sb-admin.css" rel="stylesheet">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link href="/css/plugins/morris.css" rel="stylesheet">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/plugins/morris/raphael.min.js"></script>
  <script src="js/plugins/morris/morris.min.js"></script>
  <script src="js/plugins/morris/morris-data.js"></script>
  <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <script>
    window.Laravel = {!! json_encode([
      'csrfToken' => csrf_token(),
      ]) !!};
    </script>
  </head>
  <body>

    <div id="wrapper" >
      <div class="container-fluid">
        <!-- Navigation -->
        @extends('layouts.bottom')
        <div class="container">
          <div class="row">

            <div class="col-md-9">
              <div class="container-fluid">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Orders</h3>
                  </div>
                  <div class="panel-body">
                    <a href="{{ url('/tables') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                  </br>
                  <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/orders">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                       <label class="col-sm-2 control-label">Order Number</label>
                       <div class="col-sm-3">
                         <input type="input"  id="order_number"  name="order_number" class="form-control" required/>
                       </div>
                     </div>                   
                     <div class="form-group">
                       <label class="col-sm-2 control-label">Table Number</label>
                       <div class="col-sm-4">           
                         <select class="form-control" name="table_id" id="tabel_id">
                           <option value="0" selected="true" disabled="true">Select Table</option>
                           @foreach($tables as $d)
                           <option value="{{$d->id}}">{{$d->table_number}}</option>
                           @endforeach
                         </select>
                       </div>
                     </div>                                      
                     <div class="table-responsive">
                      <table class="table table-bordered table-hover" id="TData">
                        <thead>
                          <tr>
                            <td colspan="5">
                              <label># Detail Order of Food</label>
                              <a href="javascript:void(0)" onclick="AddOrder()" title="Add New Order" class="btn pull-right btn-sm btn-success"><i class="fa fa-plus"></i> Add Order</a>
                            </td>
                          </tr>
                          <tr>
                            <th>Food</th>
                            <th>Quantity</th>                                                 
                            <th>Price</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="tbodyf">
                        </tbody>
                      </table>
                    </div>
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover" id="TDatad">
                        <thead>
                          <tr>
                            <td colspan="5">
                              <label># Detail Order of Drink</label>
                              <a href="javascript:void(0)" onclick="AddOrderd()" title="Add New Order" class="btn pull-right btn-sm btn-success"><i class="fa fa-plus"></i> Add Order</a>
                            </td>
                          </tr>
                          <tr>
                            <th>Drink</th>
                            <th>Quantity</th>                                                 
                            <th>Price</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="tbodyd">
                        </tbody>
                      </table>
                    </div>
                    <div class="pull-right">            
                      <button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                  </form>



                  <br/>
                  <br/>
                  <div class="table-responsive">
                    <table class="table table-borderless">
                      <thead>
                        <tr>
                          <th>ID</th><th>Order Number</th><th>Table Number</th><th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($orders as $order)
                        <tr>
                          <td>{{ $order->id }}</td>
                          <td>{{ $order->order_number }}</td><td>{{ $order->table_id }}</td>
                          <td>
                            <a href="{{ url('/orders/' . $order->id) }}" title="View Item"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            <a href="{{ url('/orders/' . $order->id . '/edit') }}" title="Edit Item"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/orders', $order->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Item',
                            'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div class="pagination-wrapper"> {!! $orders->appends(['search' => Request::get('search')])->render() !!} </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
@include('layouts.javascript')