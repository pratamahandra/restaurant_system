<!DOCTYPE html>
<html>
<head>
  <link href="/css/app.css" rel="stylesheet">
  <script type="text/javascript" src="/assets/js/jquery/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="/assets/js/bootstrap/js/bootstrap.min.js"></script>    
  <link type="text/css" href="/assets/css/stylesheet.css" rel="stylesheet" media="screen" />
  <link type="text/css" href="/assets/css/select2.css" rel="stylesheet" media="screen" />
  <link type="text/css" href="/assets/css/select2-bootstrap.css" rel="stylesheet" media="screen" />
  <script src="/assets/js/jquery/select2.js" type="text/javascript"></script>      
</br>

<div class="container">
  <div class="row">
    @include('admin.sidebar')

    <div class="col-md-9">
      <div class="container-fluid">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Orders : {{ $orders->id }} </h3>                       
          </div>          
          <div class="panel-body">
            <a href="{{ url('/tables') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
          </br>
          <div class="panel-body">
          <form class="form-horizontal" method="POST" action="/orders/edit">
              <input type="hidden"  id="id_edit" name="id_edit" readonly="true" value="{{ $orders->id }}" /> 
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
               <label class="col-sm-2 control-label">Order Number</label>
               <div class="col-sm-3">
                 <input type="input"  id="order_number" readonly="true" value="{{ $orders->order_number }}" name="order_number" class="form-control" required/>
               </div>
             </div>                   
             <div class="form-group">
               <label class="col-sm-2 control-label">Table Number</label>
               <select class="form-control" name="table_id" id="tabel_id">
                 <option value="0" selected="{{ $orders->table_id }}" disabled="true">Select Food</option>
                 @foreach($tables as $d)
                 <option value="{{$d->id}}" @if ($d->id == $orders->table_id) selected @endif>{{$d->table_number}}</option>
                 @endforeach
               </select>
             </div>
           <!--Tabel Pesanan-->                                     
           <div class="table-responsive">
            <table class="table table-bordered table-hover" id="TData">
              <thead>
                <tr>
                  <td colspan="5">
                    <label># Ordered Detail </label>                    
                  </td>
                </tr>
                <tr>
                  <th>Items</th>
                  <th>Quantity</th>                                                 
                  <th>Price</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id=tbody>
                <?php $v=0;?>
                <?php foreach($order_d as $or_d): ?>            
                <tr id="<?php echo $v;?>" class="index">
                  <td><select class="item form-control" name="item[<?php echo $v;?>]" >
                    @foreach($items as $keyy => $py)
                    <option value="{!!$py->id!!}" @if ($or_d->item_id == $py->id) selected @endif>{!!$py->name!!}</option>
                    @endforeach
                  </select></td>
                  <td><input class="quantityi form-control" name="quantityi[<?php echo $v;?>]" value="<?php echo $or_d->quantity;?>"></input></td>                  
                  <td><input class="pricei form-control" name="pricei[<?php echo $v;?>]" value="<?php echo $or_d->price;?>" readonly="true"></input></td>                  
                  <td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>
                </tr>
                <?php $v++; EndForeach; ?>
              </tbody>
            </table>   
            <!--Tabel tambahan Makanan-->
            <table class="table table-bordered table-hover" id="TDataf">
              <thead>
                <tr>
                  <td colspan="5">
                    <label># Detail Order of Food</label>
                    <a href="javascript:void(0)" onclick="AddOrder()" title="Add New Order" class="btn pull-right btn-sm btn-success"><i class="fa fa-plus"></i> Add Order</a>
                  </td>
                </tr>
                <tr>
                  <th>Items</th>
                  <th>Quantity</th>                                                 
                  <th>Price</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="tbodyf">
              </tbody>
            </table>
            
            <!---Tabel Tambahan minuman-->          
            <table class="table table-bordered table-hover" id="TDatad">
              <thead>
                <tr>
                  <td colspan="5">
                    <label># Detail Order of Drink</label>
                    <a href="javascript:void(0)" onclick="AddOrderd()" title="Add New Order" class="btn pull-right btn-sm btn-success"><i class="fa fa-plus"></i> Add Order</a>
                  </td>
                </tr>
                <tr>
                  <th>Drink</th>
                  <th>Quantity</th>                                                 
                  <th>Price</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="tbodyd">
              </tbody>
            </table>
          </div>          
          <div class="pull-right">            
            <button type="submit" data-toggle="tooltip" title="Update" class="btn btn-primary"><i class="fa fa-save"></i> update</button>
          </div>
        </form>
      </div>
    </div>

    <script type="text/javascript">        
    function AddOrder(){
      //-------tambah row---------    
      var row = '<tr>'+
      '<td>'+
      '<select class="food form-control" name="food[]" id="food">'+
      '<option value="0" selected="true" disabled="true">Select Food</option>'+   
      '@foreach($foods as $key => $p)'+
      '<option value="{!!$p->id!!}">{!!$p->name!!}</option>'+
      '@endforeach'+   
      '</select>'+
      '</td>'+
      '<td>'+
      '<input class="quantity form-control" name="quantity[]"></input>'+
      '</td>'+
      '<td>'+
      '<input class="price form-control" name="price[]" readonly="true"></input>'+
      '</td>'+
      '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>'+
      '</td>'+
      '</tr>';      
      $("#tbodyf").append(row);    
    };
    $('#tbodyf').delegate(".food","change",function(){
      var row = $(this).parent().parent();
      var id = row.find('.food').val();
      var dataId = {'id':id};
      $.ajax({
        type : 'GET',
        url : '{!!URL::route("findPrice")!!}',
        dataType : 'json',
        data : dataId,
        success : function(data){
          row.find('.price').val(data.price);            
        }
      });      
    });
    function deleteRow(btn) {
      var row = btn.parentNode.parentNode;
      row.parentNode.removeChild(row);
    };
    function AddOrderd(){
      //-------tambah row---------
      var rowd = '<tr>'+
      '<td>'+
      '<select class="drink form-control" name="drink[]" id="drink">'+
      '<option value="0" selected="true" disabled="true">Select Drink</option>'+   
      '@foreach($drinks as $k => $d)'+
      '<option value="{!!$d->id!!}">{!!$d->name!!}</option>'+
      '@endforeach'+   
      '</select>'+
      '</td>'+
      '<td>'+
      '<input class="quantityd form-control" name="quantityd[]"></input>'+
      '</td>'+
      '<td>'+
      '<input class="priced form-control" name="priced[]" readonly="true"></input>'+
      '</td>'+
      '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>'+
      '</td>'+
      '</tr>';      
      $("#tbodyd").append(rowd);    
    };
    $('#tbodyd').delegate(".drink","change",function(){
      var rowd = $(this).parent().parent();
      var id = rowd.find('.drink').val();
      var dataId = {'id':id};
      $.ajax({
        type : 'GET',
        url : '{!!URL::route("findPrice")!!}',
        dataType : 'json',
        data : dataId,
        success : function(data){
          rowd.find('.priced').val(data.price);            
        }
      });      
    });
    function deleteRowd(btn) {
      var rowd = btn.parentNode.parentNode;
      rowd.parentNode.removeChild(rowd);
    };
    AddOrderd(); 
    AddOrder();    

  </script>