<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	public $timestamps = false;
	protected $fillable = ['id','order_id','item_id','price','quantity'];
}
