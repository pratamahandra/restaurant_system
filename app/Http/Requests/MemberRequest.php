<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    public function authorize()
    {
        return false;
    }
    
    public function rules()
    {
        return [
        'card_number' => 'required|min:3',
        'name'=> 'required|min:10',
        'address'=> 'required|min:10',
        'discount'=> 'required|min:10',
        ];
    }
}
