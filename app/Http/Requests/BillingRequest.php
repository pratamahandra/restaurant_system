<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BillingRequest extends FormRequest
{
   public function authorize()
    {
        return false;
    }
    
    public function rules()
    {
        return [
        'name' => 'required|min:3',
        'category'=> 'required|min:10',
        'price'=> 'required|min:10',
        ];
    }
}
