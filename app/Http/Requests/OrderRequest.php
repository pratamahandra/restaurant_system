<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
   public function authorize()
    {
        return false;
    }
    
    public function rules()
    {
        return [
        'order_number' => 'required|min:3',
        'order_date'=> 'required|min:10',
        'status'=> 'required|min:10',
        ];
    }
}
