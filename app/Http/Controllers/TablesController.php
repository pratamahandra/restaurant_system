<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Table;   
use Illuminate\Http\Request;
use Session;

class TablesController extends Controller
{    
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $tables = Table::where('table_number', 'LIKE', "%$keyword%")

            ->paginate($perPage);
        } else {
            $tables = Table::paginate($perPage);
        }

        return view('tables.index', compact('tables'));
    }

    public function create()
    {
        return view('tables.create');
    }
    
    public function store(Request $request)
    {

        $requestData = $request->all();
        
        Table::create($requestData);

        Session::flash('flash_message', 'Table added!');

        return redirect('tables');
    }

    public function show($id)
    {
        $table = Table::findOrFail($id);


        return view('tables.show', compact('table'));
    }    
    public function edit($id)
    {
        $table = Table::findOrFail($id);

        return view('tables.edit', compact('table'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();
        
        $table = Table::findOrFail($id);
        $table->update($requestData);

        Session::flash('flash_message', 'Table updated!');

        return redirect('tables');
    }

    public function destroy($id)
    {
        Table::destroy($id);

        Session::flash('flash_message', 'Table deleted!');

        return redirect('tables');
    }
}
