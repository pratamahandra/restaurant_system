<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Order;
use App\Models\Table;
use App\Models\Item;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use DateTime;
use Session;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 5;

        if (!empty($keyword)) {
            $orders = Order::where('order_number', 'LIKE', "%$keyword%")
            ->orWhere('table_id', 'LIKE', "%$keyword%")            
            ->paginate($perPage);
        } else {
            $orders = Order::orderBy('id', 'desc')->paginate($perPage);
        }
        $tables = DB::Table('tables')->get(); //get data dari form  table
        //return view('orders.create');
        $foods  = Item::where('category','0')->get();
        $drinks = Item::where('category','1')->get();
        return view('orders.index', compact('orders','foods' , $foods ,'drinks' , $drinks , 'tables' , $tables));
        

    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */   

    public function create(Request $request)

    {                   
        //dd($request->all());
        $orders= new Order;
        $orders->order_number = $request->order_number;
            //$orders->user_id = Auth::user()->id;        
        $orders->table_id = $request->table_id;    
        $today = new DateTime();
        $today = date('Y-m-d');
        $orders->order_date = $today;    
        $total_food=0;
        $total_drink=0;                            
        if($orders->save())
        {            
            if (Input::has('food'))
            {
                $id = $orders->id;
                foreach ($request->food as $key => $value)
                {    

                    $orders->orders()->attach($id, array('item_id' => $request->food [$key] ,'price' => $request->price [$key] , 'quantity' => $request->quantity [$key]));                    
                    $sub_food=$request->quantity [$key]*$request->price [$key];
                    $total_food=$sub_food+$total_food;
                    
                }
            }

            if (Input::has('drink'))
            {
                $id = $orders->id;
                foreach ($request->drink as $k => $v)
                {                

                    $orders->orders()->attach($id, array('item_id' => $request->drink [$k] ,'price' => $request->priced [$k] , 'quantity' => $request->quantityd [$k]));     
                    $sub_drink=$request->quantityd [$k]*$request->priced [$k];               
                    $total_drink=$sub_drink+$total_drink; 
                }
            }

        }           
        $TOTAL=$total_drink+$total_food;            
        $orders->sub_total = $TOTAL;
        $orders->save();    
        return redirect('orders');        

    }

    public function findPrice(Request $request)
    {
        $data = Item::select('price')->where('id',$request->id)->first();
        return response()->json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $food_id = Input::get('food_id');
        for($j=0;$j<count($food_id);$j++){
            $cek_r = Order::where('food_id','=',$food_id[$j])->first();
            if(empty($cek_r)){
                $order = new Order;
                $d_food = Food::find($food_id[$j]);                    
                $order->order_id = $id;
                $order->food_id = $food_id[$j];
                $order->price = $d_food->type->price;
                $order->save();
                $d_food->save();                    
            }
        }               

        Session::flash('flash_message', 'Order added!');

        return redirect('orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);

        return view('orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {        
        $tables = DB::Table('tables')->get(); //get data dari form  table
        //return view('orders.create');
        $foods  = Item::where('category','0')->get();
        $drinks = Item::where('category','1')->get();
        $items = Item::all();
        $orders = Order::findOrFail($id);
        $order_d = Order_detail::where('order_id','=',$id)->get();        
        $data = Order::find($id);                
        $options = array(
            'data'=>$data,
            'order_detail'=>$order_d,            
            );
        
        return view('orders.edit', compact('orders','foods' , $foods ,'drinks' , $drinks , 'tables' , $tables, $options, $order_d, 'order_d', $items ,'items' ));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();
        
        $order = Order::findOrFail($id);
        $order->update($requestData);

        Session::flash('flash_message', 'Order updated!');

        return redirect('orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Order::destroy($id);        
        Session::flash('flash_message', 'Order deleted!');

        return redirect('orders');
    }
}
