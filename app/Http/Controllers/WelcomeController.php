<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Table;   
use Illuminate\Http\Request;
use Session;

class WelcomeController extends Controller
{    
    public function index(Request $request)
    {
        $table = Table::where('status','0')->get()->count();        
        return view('welcome', compact('table'));
    }
}
