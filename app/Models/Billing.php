<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
	public $timestamps = false;  
	protected $table = 'billings';	  
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'billing_number', 'sub_total','delivery_amount', 'delivery_destination', 'member_id', 'payment_method' , 'total' ,'bil_date','status'];


    public function billing_details()
    {
    	return $this->belongsTo('App\Billing_details');
    }
    public function billing_orders()
    {
    	return $this->belongsTo('App\Billing_orders');
    }
}
