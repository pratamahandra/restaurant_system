<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingOrder extends Model
{
    public $timestamps = false;
    protected $fillable = ['id','billing_id','order_id'];

    public function ManyBillings()
    {
    	return $this->hasMany('App\Billing');
    }
    public function ManyOrders()
    {
    	return $this->hasMany('App\Order');
    }
}
