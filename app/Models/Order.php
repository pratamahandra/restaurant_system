<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{    
    public $timestamps = false; 
    protected $table = 'orders';

    protected $primaryKey = 'id';

    protected $fillable = ['order_number', 'table_id', 'sub_total', 'order_date' ];

  
    public function orders()
    {
        return $this->belongsToMany(Item::class, 'order_details')->withPivot('price' , 'quantity');
    }
    // public function billingOrders()
    // {
    //     return $this->belongsTo('App\Billing_orders');
    // }
}
