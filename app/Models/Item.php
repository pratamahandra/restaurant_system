<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	public $timestamps = false;  
	protected $table = 'items';

	protected $primaryKey = 'id';

	protected $fillable = ['name', 'category', 'price'];

	public function items()
	{
		return $this->belongsToMany(Order::class, 'order_details', 'item_id', 'order_id')->withPivot('price', 'quantity');
	}
}
